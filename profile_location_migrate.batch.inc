<?php

/**
 * @file
 * 
 * Batch callbacks for the profile_location_migrate module.
 *
 * @author: Elliott Foster
 * @copyright: Industry Trader 2010
 */

/**
 * Returns the confirmation form to run the migration.
 */
function profile_location_migrate_migrate() {
  $form = array();

  $form = confirm_form(
    $form,
    t('Migrate user data to content profiles'),
    'admin/user/profile_location_migrate',
    t('All user data will be migrated to content profiles. This action cannot be undone.'),
    t('Migrate'),
    t('Cancel')
  );

  return $form;
}

function profile_location_migrate_migrate_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/user/profile_location_migrate';

  $batch = array(
    'title' => t('Migrating user location data to content profiles'),
    'operations' => array(
      array('profile_location_migrate_batch', array())
    ),
    'finished' => 'profile_location_migrate_finished',
    'file' => drupal_get_path('module', 'profile_location_migrate') . '/profile_location_migrate.batch.inc',
  );
  batch_set($batch);
}

/** 
 * Processes the migration in batch.
 */
function profile_location_migrate_batch(&$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_user'] = 0;
    $context['sandbox']['types'] = content_profile_get_types('names');
    $context['sandbox']['targets'] = variable_get('profile_location_migrate_fields', array());
    $context['sandbox']['max'] = db_result(db_query(
      "SELECT COUNT(DISTINCT u.uid) FROM {users} u LEFT JOIN {node} n ON n.uid=u.uid WHERE n.type IN (" .
        db_placeholders($context['sandbox']['types'], 'varchar') .
        ") AND u.uid > %d",
      array_merge($context['sandbox']['types'], array($context['sandbox']['current_user']))
    ));
  }
  $limit = 20;
  $res = db_query_range(
    "SELECT DISTINCT u.uid FROM {users} u LEFT JOIN {node} n ON n.uid=u.uid WHERE n.type IN(" .
      db_placeholders($context['sandbox']['types'], 'varchar') .
      ") AND u.uid > %d ORDER BY u.uid ASC",
    array_merge($context['sandbox']['types'], array($context['sandbox']['current_user'])),
    0,
    $limit
  );
  while ($account = db_fetch_object($res)) {
    $account = user_load($account->uid);

    foreach ($context['sandbox']['types'] as $type => $type_name) {
      $profile = content_profile_load($type, $account->uid);
      if (!$profile) {
        continue;
      }

      foreach ($context['sandbox']['targets'] as $target) {
        if (strpos($target, $type) === FALSE) {
          continue;
        }

        if ($target == $type . ':location') {
          foreach ($account->locations as $k => $location) {
            $profile->locations = $account->locations;
          }
        }
        else if (strpos($target, ':location_cck:')) {
          list($t, $l, $field) = explode(':', $target);

          if (isset($profile->{$field})) {
            foreach ($account->locations as $k => $location) {
              $profile->{$field}[$k] = array(
                'lid'           => $location['lid'],
                'province_name' => $location['province_name'],
                'country_name'  => $location['country_name'],
              );
            }
          }
        }
      }

      node_save($profile);
    }

    $context['results'][] = $account->uid .' : '. $account->name;
    $context['sandbox']['progress']++;
    $context['sandbox']['current_user'] = $account->uid;
    $context['message'] = $account->name;
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Returns the finished messages.
 */
function profile_location_migrate_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'One user profile location migrated.', '@count user profile locations migrated.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}

