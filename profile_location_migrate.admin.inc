<?php

/**
 * @file
 * 
 * Admin callbacks for the profile_location_migrate module.
 * 
 * @author: Elliott Foster
 * @copyright: Industry Trader 2010
 */

/**
 * Returns the system settings form.
 */
function profile_location_migrate_settings() {
  $form = array();
  $options = array();

  $types = content_profile_get_types();
  foreach ($types as $name => $type) {
    // check to see if this node type either supports locations
    // or has a location cck field
    $settings = variable_get('location_settings_node_' . $name, array());
    if (isset($settings['multiple']) && $settings['multiple']['min'] > 0) {
      $options[$name . ':location'] = t('@name location field', array('@name' => $name));
    }

    // now check for location cck stuff
    if (module_exists('location_cck')) {
      $fields = content_fields(NULL, $name);
      foreach ($fields as $field_name => $field) {
        if ($field['type'] == 'location') {
          $options[$name . ':location_cck:' . $field_name] = t('@type Location CCK @name field', array('@type' => $type->name, '@name' => $field['widget']['label']));
        }
      }
    }
  }

  $form['profile_location_migrate_fields'] = array(
    '#type' => 'select',
    '#title' => t('Field Mappings'),
    '#options' => $options,
    '#default_value' => variable_get('profile_location_migrate_fields', array()),
    '#description' => t('Select the destination(s) for the location data to be migrated to'),
    '#multiple' => TRUE,
  );

  return system_settings_form($form);
}

